package  
{
	import Box2D.Collision.Shapes.*;
	import Box2D.Common.Math.*;
	import Box2D.Dynamics.*;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.*;
	import mx.core.BitmapAsset;
	import org.flixel.*;
	
	public class Island extends FlxB2Sprite 
	{
		
		public function Island(W:b2World, X:Number, Y:Number, Img:Class) 
		{
			super(W, X, Y);
			loadGraphic(Img, false, false, 0, 0, true);
			b2UserData = Globals.ISLAND;
			b2Friction = 2;
			b2Restitution = 0;
			createBody();
			var verts:Array = detectVertices();
			for (var i:int = 2; i < verts.length; i++ )
			{
				var p:Point = verts[0];
				var a:Point = verts[i - 1];
				var b:Point = verts[i];
				createPolygon([p.x, p.y, a.x, a.y, b.x, b.y]);
				createFixture();
			}
			var swap:BitmapData = (new Globals.NOISE_IMG() as BitmapAsset).bitmapData;
			swap.threshold(pixels, swap.rect, new Point(), "<=", 0xFF000000, 0);
			pixels.draw(swap, null, null, "multiply");
			swap.dispose();
			dirty = true;
		}
		override public function createPolygon(vertices:Array):void
		{
			var vrts:Array = new Array();
			var poly:b2PolygonShape = new b2PolygonShape();
			
			if (vertices.length % 2 == 1)
				throw("Error: createPolygon: Wrong vertex array");
			
			for (var i:int = 0; i < vertices.length; i+=2)
				vrts.push(new b2Vec2(vertices[i] / FlxG.B2SCALE, vertices[i + 1] / FlxG.B2SCALE));
			
			poly.SetAsArray(vrts, vrts.length);
			b2FixtureShape = poly;
		}
		override public function createBody():void
		{
			bodyDef = new b2BodyDef();
			bodyDef.userData = b2UserData;
			bodyDef.angle = b2Angle;
			bodyDef.position = b2Position.Copy();
			bodyDef.linearVelocity = b2LinearVelocity.Copy();
			bodyDef.angularVelocity = b2AngularVelocity;
			bodyDef.linearDamping = b2LinearDamping;
			bodyDef.angularDamping = b2AngularDamping;
			bodyDef.allowSleep = b2AllowSleep;
			bodyDef.awake = b2IsAwake;
			bodyDef.fixedRotation = b2FixedRotation;
			bodyDef.bullet = b2IsBullet;
			bodyDef.type = b2Type;
			bodyDef.active = b2IsActive;
			body = world.CreateBody(bodyDef);
		}
		public function createFixture():b2Fixture
		{
			// fixture is bogus
			fixtureDef = new b2FixtureDef();
			fixtureDef.shape = b2FixtureShape;
			
			fixtureDef.friction = b2Friction;
			fixtureDef.restitution = b2Restitution;
			fixtureDef.density = b2Density;
			fixtureDef.filter = b2Filter.Copy();
			fixtureDef.isSensor = b2IsSensor;
			return body.CreateFixture(fixtureDef);
		}
		private function detectVertices():Array
		{
			var bottom:Point = new Point();
			var inter:Array = new Array();
			
			var brect:Rectangle = pixels.getColorBoundsRect(0xFFFFFFFF, Globals.GROUND_COLOR);
			
			// find bottom point
			for (var i:int = 0; i < brect.width; i++)
				if (pixels.getPixel32(brect.x + i, brect.y + brect.height-1) == Globals.GROUND_COLOR)
				{
					bottom.x = brect.x + i - pixels.width / 2;
					bottom.y = brect.y + brect.height - 1 - pixels.height / 2;
					break;
				}
			// find intermediate points
			const STEP_SIZE:int = 16;
			for (i = brect.x; i < brect.right; i+= Math.min(STEP_SIZE, Math.max(1,brect.right-i-1)))
				for (var j:int = 0; j < pixels.height; j++)
					if (pixels.getPixel32(i, j) == Globals.GROUND_COLOR)
					{
						inter.push(new Point((i - pixels.width / 2), (j - pixels.height / 2)));
						break;
					}
			inter.unshift(bottom);
			return inter;
		}
		override public function update():void 
		{
			super.update();
		}
	}
}