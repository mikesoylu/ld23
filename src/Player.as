package  
{
	import org.flixel.*;
	import Box2D.Dynamics.*;
	import Box2D.Common.Math.*;
	
	public class Player extends FlxB2Sprite 
	{
		public var isTouchingGround:Boolean = false;
		public var currentAnimation:String = "";
		private var jump_cooldown:int = 0;
		
		public function Player(W:b2World, X:Number, Y:Number) 
		{
			super(W, X, Y);
			
			loadGraphic(Globals.PLAYER_IMG, true, true, 28, 40);
			offset.y = 8;
			addAnimation("idle", [0, 1], 2);
			addAnimation("run", [3, 4, 5, 6], 8);
			addAnimation("jump", [2]);
			addAnimation("attack", [7, 0], 4, false);
			play("idle");
			currentAnimation = "normal";
			fixed = true;
			
			// setup physics
			b2Type = b2Body.b2_dynamicBody;
			b2Friction = 2;
			b2Restitution = 0;
			b2LinearDamping = 0.5;
			b2AllowSleep = false;
			b2UserData = Globals.PLAYER;
			createCircle(16);
		}
		
		override public function update():void 
		{
			super.update();
			var flag:Boolean = false;
			body.SetFixedRotation(false);
			jump_cooldown++;
			if (FlxG.keys.justPressed("UP") && jump_cooldown < 8)
			{
				body.ApplyImpulse(new b2Vec2(0, -30), body.GetPosition());
				jump_cooldown = 8;
			}

			if (FlxG.keys.LEFT)
			{
				if (isTouchingGround)
					body.SetAngularVelocity(-8);
				else
					body.ApplyForce(new b2Vec2( -10, 0), body.GetPosition());
				facing = LEFT;
				flag = true;
			}
			if (FlxG.keys.RIGHT)
			{
				if (isTouchingGround)
					body.SetAngularVelocity(8);
				else
					body.ApplyForce(new b2Vec2( 10, 0), body.GetPosition());
				facing = RIGHT;
				
				flag = true;
			}
			if (isTouchingGround)
				jump_cooldown = 0;
			if (!flag)
			{
				body.SetAngularVelocity(0);
				body.SetFixedRotation(true);
			}
			if (FlxG.keys.justPressed("SPACE"))
			{
				play("attack");
				currentAnimation = "attack";
			}
			if (!(currentAnimation == "attack" && !finished))
			{
				if (!flag && isTouchingGround)
				{
					play("idle");
					currentAnimation = "normal";
				} else if (flag && isTouchingGround)
				{
					play("run");
					currentAnimation = "normal";
				} else if (jump_cooldown > 25)
				{
					play("jump");
					currentAnimation = "jump";
				}
			}
		}
	}
}