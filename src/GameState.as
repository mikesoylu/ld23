package  
{
	import mx.core.FlexSprite;
	import org.flixel.*;
	import Box2D.Dynamics.*;
	import flash.display.*;
	import Box2D.Common.Math.b2Vec2;

	public class GameState extends FlxB2State 
	{
		private var player:Player;
		private var debugLayer:Sprite;
		private var shout:FlxSprite;
		private var shouting:Boolean = false;
		private var monolith:FlxB2Sprite;
		private var weapon:FlxSprite;
		private var weapon_flash:FlxSprite;
		
		override public function create():void 
		{
			super.create();
			
			FlxG.bgColor = 0xFF6DC2FF;
			
			// debug stuff
			debugLayer = new Sprite();
			var debugDraw:b2DebugDraw = new b2DebugDraw();
			debugDraw.SetSprite(debugLayer);
			debugDraw.SetDrawScale(FlxG.B2SCALE);
			debugDraw.SetLineThickness(1.0);
			debugDraw.SetAlpha(1);
			debugDraw.SetFillAlpha(0.4);
			debugDraw.SetFlags(b2DebugDraw.e_shapeBit);
			world.SetDebugDraw(debugDraw);
			
			// background
			var bground:FlxSprite = new FlxSprite(0, 0, Globals.SKY_BACKGROUND_IMG);
			bground.scrollFactor.y = 0;
			bground.scrollFactor.x = 0;
			add(bground);
			bground = new FlxSprite(0, 0, Globals.CLOUDS_BACKGROUND_IMG);
			bground.scrollFactor.y = 0;
			bground.scrollFactor.x = 0.2;
			bground.y = 120;
			add(bground);
			bground = new FlxSprite(0, 0, Globals.ISLANDS_BACKGROUND_IMG);
			bground.scrollFactor.y = 0;
			bground.scrollFactor.x = 0.25;
			bground.x = 200;
			bground.y = 100;
			add(bground);
			
			monolith = new FlxB2Sprite(world, 300, 0, Globals.MONOLITH_IMG);
			monolith.b2Type = b2Body.b2_dynamicBody;
			monolith.b2FixedRotation = true;
			monolith.b2Friction = 1;
			monolith.b2Restitution = 0;
			monolith.createBox(30, 46);
			monolith.scrollFactor.y = 0;
			add(monolith);
			
			var island:Island = new Island(world, 80, 200, Globals.ISLAND01_IMG);
			island.scrollFactor.y = 0;
			add(island);
			
			island = new Island(world, 280, 200, Globals.ISLAND02_IMG);
			island.scrollFactor.y = 0;
			add(island);
			
			island = new Island(world, 550, 190, Globals.ISLAND03_IMG);
			island.scrollFactor.y = 0;
			add(island);
			
			island = new Island(world, 870, 210, Globals.ISLAND04_IMG);
			island.scrollFactor.y = 0;
			add(island);
			
			player = new Player(world, 100, 0);
			player.scrollFactor.y = 0;
			player.addAnimationCallback(changeWeaponAnim);
			add(player);
			
			weapon = new FlxSprite(FlxG.width / 2-3, player.y);
			weapon.loadGraphic(Globals.GUN_SHEET_IMG, true, true, 28, 40);
			weapon.offset.y = 8;
			weapon.addAnimation("normal", [0, 1], 2);
			weapon.addAnimation("jump", [2]);
			weapon.addAnimation("attack", [3, 4], 4, false);
			weapon.scrollFactor.y = 0;
			weapon.solid = false;
			add(weapon);
			
			weapon_flash = new FlxSprite(FlxG.width / 2-3, player.y);
			weapon_flash.loadGraphic(Globals.WEAPON_FLASH_IMG, true, true, 28, 24);
			weapon_flash.offset.y = -6;
			weapon_flash.addAnimation("strike", [9, 0, 1, 2], 12, false);
			weapon_flash.addAnimation("fire", [9, 3, 4, 5], 12, false);
			weapon_flash.addAnimation("cast", [9, 6, 7, 8], 12, false);
			weapon_flash.addAnimation("none", [9], 0, false);
			weapon_flash.play("none");
			weapon_flash.scrollFactor.y = 0;
			weapon_flash.solid = false;
			add(weapon_flash);
			
			
			shout = new FlxSprite(FlxG.width / 2-3, player.y);
			shout.loadGraphic(Globals.SHOUT_IMG, true, false, 37, 18);
			shout.scrollFactor.x = 0;
			shout.scrollFactor.y = 0;
			shout.visible = false;
			shout.solid = false;
			add(shout);
			
			bground = new FlxSprite(0, 0, Globals.CLOUDS_FOREGROUND_IMG);
			bground.scrollFactor.y = 0;
			bground.scrollFactor.x = 2;
			bground.scale.x = 4;
			bground.alpha = 0.75
			bground.scale.y = 2;
			bground.x = 300;
			bground.y = 280;
			add(bground);
			
			FlxG.camera.follow(player);
			
			world.SetGravity(new b2Vec2(0, Globals.GRAVITY));
			world.SetContactListener(new ContactListener(player));
		}
		private function changeWeaponAnim(n:String, f:uint, fi:uint):void
		{
			weapon.play(player.currentAnimation);
			if (player.currentAnimation == "attack")
				weapon_flash.play("cast");
			else
				weapon_flash.play("none");
		}
		override public function draw():void 
		{
			super.draw();
			if (FlxG.visualDebug)
				FlxG.camera.buffer.draw(debugLayer);
		}
		override public function update():void 
		{
			super.update();
			
			weapon.y = player.y;
			weapon.x = player.x;
			weapon.facing = player.facing;
			
			weapon_flash.y = player.y;
			weapon_flash.x = player.x;
			weapon_flash.facing = player.facing;
			if (player.facing == 0x0001)
				weapon_flash.offset.x = 10;
			else
				weapon_flash.offset.x = -10;
			
			if (!shouting && FlxG.random() < 0.001)
			{
				shout.randomFrame();
				shouting = true;
			}
			if (shouting)
			{
				shout.y = player.y - 20;
				shout.visible = true;
				if (FlxG.random() < 0.01)
					shouting = false;
			} else
				shout.visible = false;
			
			if (FlxG.visualDebug)
				world.DrawDebugData();
		}
	}
}