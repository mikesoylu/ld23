package  
{
	public class Globals 
	{
		// constants
		public static const GRAVITY:Number = 20;
		public static const GROUND_COLOR:uint = 0xFFB5884A;
		public static const PLAYER:uint = 1;
		public static const ISLAND:uint = 2;
		
		// assets
		[Embed(source="../assets/island_bg.png")]
		public static const ISLANDS_BACKGROUND_IMG:Class;
		[Embed(source="../assets/sky_bg.png")]
		public static const SKY_BACKGROUND_IMG:Class;
		[Embed(source="../assets/cloud_bg.png")]
		public static const CLOUDS_BACKGROUND_IMG:Class;
		[Embed(source="../assets/front_cloud.png")]
		public static const CLOUDS_FOREGROUND_IMG:Class;
		

		[Embed(source="../assets/islands/island01.png")]
		public static const ISLAND01_IMG:Class;
		[Embed(source="../assets/islands/island02.png")]
		public static const ISLAND02_IMG:Class;
		[Embed(source="../assets/islands/island03.png")]
		public static const ISLAND03_IMG:Class;
		[Embed(source="../assets/islands/island04.png")]
		public static const ISLAND04_IMG:Class;
		
		[Embed(source="../assets/weapons/gun.png")]
		public static const GUN_SHEET_IMG:Class;
		[Embed(source="../assets/weapons/bazooka.png")]
		public static const BAZOOKA_SHEET_IMG:Class;
		[Embed(source="../assets/weapons/sword.png")]
		public static const SWORD_SHEET_IMG:Class;
		[Embed(source="../assets/weapons/wand.png")]
		public static const WAND_SHEET_IMG:Class;
		
		[Embed(source="../assets/weapons/weapon_flash.png")]
		public static const WEAPON_FLASH_IMG:Class;
		
		[Embed(source="../assets/shouts.png")]
		public static const SHOUT_IMG:Class;
		[Embed(source="../assets/monolith.png")]
		public static const MONOLITH_IMG:Class;
		
		[Embed(source="../assets/player_outlined.png")]
		public static const PLAYER_IMG:Class;
		
		[Embed(source="../assets/fractal_simplex.png")]
		public static const NOISE_IMG:Class
	}
}