package  
{
	import Box2D.Collision.b2Manifold;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2ContactImpulse;
	import Box2D.Dynamics.b2ContactListener;
	import Box2D.Dynamics.b2Fixture;
	import Box2D.Dynamics.Contacts.b2Contact;
	
	public class ContactListener extends b2ContactListener 
	{
		private var player:Player;
		public function ContactListener(P:Player)
		{
			super();
			player = P;
		}
		override public function PreSolve(contact:b2Contact, oldManifold:b2Manifold):void 
		{
			super.PreSolve(contact, oldManifold);
			player.isTouchingGround = false;
		}
		override public function PostSolve(contact:b2Contact, impulse:b2ContactImpulse):void 
		{
			super.PostSolve(contact, impulse);
			var bd:uint = contact.GetFixtureA().GetBody().GetUserData();
			var ad:uint = contact.GetFixtureB().GetBody().GetUserData();
			
			if ((ad == Globals.PLAYER || bd == Globals.PLAYER) && contact.GetManifold().m_localPlaneNormal.y < -0.5)
				player.isTouchingGround = true;
		}
	}
}